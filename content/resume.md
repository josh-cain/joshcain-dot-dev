+++ 
draft = false
title = "Resume"
slug = "resume" 
+++


# Experience
*********************************
#### Staff Software Engineer
Auth0 / Okta, Remote USA (2019 - present)

Contribute to teams that implemented new standards-based protocols for our authentication server offerings, and identify anomalous or risky behavior for millions of auth transactions every day.  Examples include:

 - [Sign in with Apple](https://auth0.com/explore/sign-in-with-apple/)
 - [Adaptive MFA](https://auth0.com/blog/auth0-introduces-adaptive-mfa/)
 - [Suspicious IP Threshold Management](https://auth0.com/changelog#7lw4mv99tpbiuXScyeaRM3)
 - [Security Center](https://auth0.com/blog/okta-cic-adds-security-center-to-attack-protection/)

#### Senior Software Applications Engineer
Red Hat, Remote USA (2015 - 2019)

As a Senior Engineer on the Identity Systems team for Red Hat, was responsible for maintaining and developing internal and external-facing SSO systems for both employees and customers. Duties sprawled from writing puppet scripts for system deployment and maintenance, to contributing code to upstream open source projects. Additionally, the team maintained a four-9 (99.99%) uptime, performed monitoring, and responded to any high severity incidents.  Examples of work:

- [Docker Authentication with Keycloak](https://developers.redhat.com/blog/2017/10/31/docker-authentication-keycloak)
- [Red Hat SSO Portal](https://sso.redhat.com/auth/realms/redhat-external/account)

#### Senior Software Engineer
BenefitFocus, Charleston SC (2013 - 2015)

- Implement software solutions across a wide platform to include J2EE web application development, mass inbound/outbound batch processes, and partner web services integration in a multi-tenant environment.
- Enhance code quality through the establishment of coding standards, documentation, curriculum development, and training. Also includes rolling out tools, and providing guidance to multiple globally distributed teams.
- Maintain batch process performance and stability through metrics storage, aggregation, and profiling operations.

#### Software Security Engineer 
Space and Naval Warfare Center (SPAWAR), Charleston SC (2011 - 2013)

- Contribute to the planning, architecture, implementation, and testing of J2EE security software on Open Source technologies and standards including RHEL, JBoss EAP, PicketLink, SAML, and XACML. Software has now been open-sourced as SecurePaas.
- Perform research and develop prototype systems according to sponsor requirements to determine feasibility of emerging technologies 
- Aid in the establishment and maintenance of coding standards and developer workflow
