---
title: Time to Un-Google My Life
subtitle: Big brother might be corporations, not governments
date: 2019-11-23
tags: ["google", "privacy"]
---

It's 2019, and the world has learned both the value and effects of personal data.  Economically, data has surpassed oil in terms of value.  Societally, issues ranging from eliction meddling to suicide and depression have been directly linked to the ability of corporations to interpret and actionize your data.  As such, I have re-evaluated my online data footprint.

## Once Upon a Time
Google appeared to be a benign tech company, who wanted to help the world by matching relevant content providers to interested users.  Google formerly held the motto "don't be evil", and made it a point to provide services that empowered users (I.E. Google Takeout).  What's more, ignorance on the part of consumers (like myself) around the depth of collection and amount of influence the data was capable of yielding helped to color Google's business model as mutually beneficial for consumers and businesses.

## Things Changed.  Fast.
However, this rosy outlook around the use of big data by entities like Google or Facebook did not last.  Societal issues have now surfaced around election meddling, echo chambers, and proliferation of false ideas based on fear and misinformation.  Furthermore, on an individual level, trends in anxiety, depression, and suicide have been linked to use of platforms with extensive knowledge of users.  Finally, the practice of doxxing and an increased politicization of the workplace makes having a complete picture of one's data in a single location an increased risk*.

\* _Author's note: One might ask "What's the risk here if you don't hold crazy beliefs?".  I would simply point out that beliefs about hot-button social issues that were mainstream (and openly expressed) 10 or 20 years ago are anathema today.  Unlike one's verbally expressed opinions, which bear both a richer context and ephemeral nature, data lives forever.  With the inability to predict where the prevailing cultural sentiment will land in the future, simply having opinions and acting in accordance with them is enough to put one at future risk._

## Don't Give Google the Keys to the Kingdom
In light of new information, I'm increasingly uncomfortable with the complete data footprint I've given Google.  They have access to everything: emails, documents, pictures, search history, and my calendar.  Basically enough to create a complete picture of my life, which gives them an incredible power of influence over my thoughts and actions.

This is why **I'm un-Googling my life**.  I'm moving my data around so as to:

 * Choose a more privacy-centric provider when possible
 * Not leave a comprehensive view of my data in any one place

## The Process
I've spent the last 10-15 years building my life around Google services, so the un-Googling process is going to be lengthy and difficult.  Some conclusions I've landed on:

 * **It won't be as convenient**: Google has done a great job of weaving together services in a consistent and seamless experience.  I'm going to have to leave this behind, as well as bits of functionality here or there.
 * **It's going to cost money**: Since Google is providing services at the cost of my data, other providers must be compensated in other ways in order to keep the lights on.  This means I'll have to pay other providers in order to keep my information private.
 * **There may not be privacy-focused alternatives**: In some cases, privacy-centric providers won't have the same feature set, or exist at all.  In these cases, I'm willing to use less-than-ideal services as long as it serves the purpose of fragmenting my data and decoupling my life from a single provider.
 * **It's going to be worth it**!

 I'll follow-up with additional posts on individual items, but here's what the process will look like at a high level:

![de-google intro image](../../images/deGooglify.png)
