---
title: View JWKS Certificate Contents from the Command Line
subtitle: Because the x5c value is not human parseable
date: 2019-09-27
tags: ["jwks", "openssl"]
---

The following code will retrieve, parse, and display an X509 certificate used for JWS

```
curl https://jcain0.auth0.com/.well-known/jwks.json | jq -r '.keys[0].x5c[0]' | base64 --decode | openssl x509 -inform DER -text -noout
```

Hope that helps someone out there with debugging!
