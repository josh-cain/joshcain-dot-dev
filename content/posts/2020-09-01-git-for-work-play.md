---
title: Git Configs for Work and Play
subtitle: because who has just one GPG Key anyway?
date: 2020-09-01
tags: ["git", "config", "gpg"]
---

Most developers have work accounts and personal accounts - often with different security and privacy concerns.  In order to keep work and play separate, here are some quick config mechanisms for isolating git configurations.

## Segregate by Directory

The first step is to organize your development environment into a folder hierarchy that clearly delineates between work and play.  Here’s what mine looks like:

```
~/code/acme      #for work
~/code/personal  #for play
```

Next, we’ll want to create a work-only git configuration profile.  I did this by creating a new file named .gitconfig-acme with the following contents:

```
[user]
	name = Josh Cain
	email = josh.cain@acme.com
	signingKey = ABC123
[commit]
	gpgsign = true
```

This contains work-specific items like my email or work-only GPG key (because everyone uses them for personal email too, right?).

## Conditionally Load Based on Context

The magic happens with git’s [includeIf](https://git-scm.com/docs/git-config/2.15.4#_conditional_includes) statment.  This statement allows for the conditional loading of a file, based on the current git repository path:

```
[user]
        name = Josh Cain
        email = josh@personal.email
        signingKey = def456
[pull]
        rebase = false
[gpg]
        program = gpg
        
[includeIf "gitdir:~/code/acme/"]
  path = ./.gitconfig-acme
```

Bottom line: As long as I keep work things in the work directory, I can rest assured that my git commits and settings are appropriate!
