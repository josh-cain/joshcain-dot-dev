---
title: Who Is This Guy Anyway?
subtitle: Meet Josh and learn about his professional interests
comments: false
---

My name is Josh Cain, and here are a few of my professional competencies:

- I love to write elegant software
- I enjoy security
- I'm an avid Linux user, who hates making Dan Walsh weep

### My History

I've worked in educational research, help a top secret clearance in the DOD world, rolled out healthcare solutions at scale during the Affordable Care Act, and learned the virtues of Open Source software at Red Hat.  Most recently I've been working at Auth0 where I've gotten to play in the implementation of identity protocols as well as anomaly detection and countermeasures.
